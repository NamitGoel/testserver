require('dotenv').config();
const express = require("express")
const bodyParser = require("body-parser")
const {createUrl} = require("./helpers/encrypt")
const {uploadDiv} = require("./helpers/headlessBrowser")

const app = express()
const port = 6942
var urlencodedParser = bodyParser.urlencoded({ extended: false })

app.use('/public', express.static('public'));
app.set("view engine", 'ejs');
app.use(bodyParser.urlencoded({extended:false}))

app.get('/', (req, res) => {
  res.render("home", {callHyperServices: false})
})

app.post('/post', (req, res) => {
  res.render("home", {callHyperServices: true});
})

app.get('/pupp/:clientId', async (req, res) => {
  var clientId = req.params.clientId;
  var url = await createUrl(clientId);
  var result = await uploadDiv(url, clientId);
  console.log(result);
  res.send(result)
})

app.listen(port, () => {
  console.log(`Listening on ${port}`)
})