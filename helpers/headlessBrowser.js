const res = require("express/lib/response");
const puppet = require("puppeteer");
const { uploadFile } = require("./s3Upload");

exports.uploadDiv = async (url, clientId) => {
  var error = "";
  try {
    const browser = await puppet.launch({ args: ["--no-sandbox"] });
    // const browser = await puppet.launch({ headless: false });
    const page = await browser.newPage();

    await page.setViewport({
      width: 504,
      height: 1137,
    });

    await page.goto(url, {
      waitUntil: "networkidle0",
    });

    const div = await page.evaluate(() => {
      return document.getElementById("content").innerHTML;
    });

    const vdom = await page.evaluate(() => {
      return vdom;
    });

    const result = await uploadFile(div, clientId, ".html");
    const result2 = await uploadFile(
      JSON.stringify(vdom),
      "vdom_" + clientId,
      ".json"
    );

    error = "success";
    // await browser.waitForTarget(() => false);
    await browser.close();
  } catch (e) {
    error = e;
  }
  return error;
};
