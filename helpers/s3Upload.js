const S3 = require("aws-sdk/clients/s3")

const bucketName = process.env.AWS_BUCKET_NAME

exports.uploadFile = (data, fileName, type) => {
  const uploadParams = {
    Bucket : bucketName,
    Body : data ? data : "",
    Key : fileName + type
  }
  return (new S3()).upload(uploadParams).promise();
}
