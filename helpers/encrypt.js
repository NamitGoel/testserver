var crypto = require("crypto");

const SECRET_KEY = {
  alg: "A128CBC",
  ext: true,
  k: "9qvGlV0DZVpVEnynWJ6kew",
  key_ops: ["encrypt", "decrypt"],
  kty: "oct",
};
const CRYPT = crypto.webcrypto;
var urlMain;

function generateInitiatePayload(clientId) {
  const initiatePayload = {
    action: "initiate",
    clientId: clientId,
    merchantId: clientId,
    merchantKeyId: "4794",
    signature: "",
    signaturePayload:
      '{"merchant_id":"' +
      clientId +
      '","customer_id":"cst_cwxynfn7zlp9krto","customer_phone":"7976719949","customer_email":"test@gmail.com","first_name":"Test","last_name":"Customer","timestamp":"1649153363482"}',
    environment: "sandbox",
    integrationType: "redirection",
  };

  const sdkPayload = {
    service: "in.juspay.hyperpay",
    requestId: "ba05f1bd-4d01-4bb8-94bd-129e9c387aa7",
    payload: initiatePayload,
  };
  return sdkPayload;
}

function generateProcessPayload(clientId) {
  const processPayload = {
    requestId: "5290a4f2-57cb-40fd-ba7d-3676b2599dba",
    service: "in.juspay.hyperpay",
    payload: {
      action: "paymentPage",
      merchantId: clientId,
      clientId: clientId,
      orderId: "DW-5aEM7ug8uu",
      amount: "1.00",
      customerId: "9521420803",
      customerEmail: "test007@gmail.com",
      customerMobile: "9521420803",
      orderDetails:
        '{"order_id":"DW-5aEM7ug8uu","first_name":"Test","last_name":"Customer","customer_phone":"7976719949","customer_email":"test@gmail.com","merchant_id":"' +
        clientId +
        '","amount":"100000.00","customer_id":"cst_cwxynfn7zlp9krto","timestamp":"1649153586087","return_url":"https://sandbox.juspay.in/end","currency":"INR","mandate.start_date":"1649153586087","mandate.end_date":"2134731745451"}',
      signature: "",
      merchantKeyId: "4794",
      language: "english",
    },
  };
  return processPayload;
}

exports.createUrl = async (clientId) => {
  await getTheUrl(
    generateInitiatePayload(clientId),
    generateProcessPayload(clientId)
  );
  return urlMain;
}

function getTheUrl(initiatePayload, processPayload) {
  var resolve = function (params) {
    urlMain = params;
    return params;
  };
  var obj = {
    initPayload: initiatePayload,
    processPayload: processPayload,
  };
  var enc,
    url = "https://juspaytest.s3.ap-south-1.amazonaws.com/",
    isBeta = false;
  try {
    aesEncrypt(JSON.stringify(obj)).then((encryptedPaylod) => {
      enc = encodeURIComponent(encryptedPaylod);
      url = url + "hyper.html?e=" + enc + "&beta=" + isBeta;
      resolve(url);
    });
  } catch (err) {
    console.info(
      "HyperSDK - Failed to encrypt, will fall back to using plain data in query params.",
      err
    );
    var encodedInitiatePayload = encodeURIComponent(
        JSON.stringify(initiatePayload)
      ),
      encodedProcessPayload = encodeURIComponent(
        JSON.stringify(processPayload)
      );
    url =
      url +
      "hyper.html?initPayload=" +
      encodedInitiatePayload +
      "&processPayload=" +
      encodedProcessPayload +
      "&beta=" +
      isBeta;
    resolve(url);
  }
}

function constructCryptoKey() {
  return CRYPT.importKey(
    "jwk",
    window.crypto.webkitSubtle
      ? new TextEncoder().encode(JSON.stringify(SECRET_KEY))
      : SECRET_KEY,
    { name: "AES-CBC" },
    true,
    ["encrypt", "decrypt"]
  );
}

function aesEncrypt(payload) {
  const crypto = window.crypto ? window.crypto : window.msCrypto;
  const iv = crypto.getRandomValues(new Uint8Array(16));
  try {
    return constructCryptoKey()
      .then((key) =>
        CRYPT.encrypt(
          { name: "AES-CBC", iv },
          key,
          new TextEncoder().encode(payload)
        )
      )
      .then((cipher) => {
        const op = stringFromArray(iv.buffer) + stringFromArray(cipher);
        return stringToBase64Url(op);
      });
  } catch {}
}

const stringFromArray = (arr) => {
  arr = arrayish(arr);
  var r = "";
  for (var i = 0; i < arr.length; i++) {
    r += String.fromCharCode(arr[i]);
  }
  return r;
};

const arrayish = (arr) => {
  if (arr instanceof Array) {
    return arr;
  }
  if (arr instanceof Uint8Array) {
    return arr;
  }
  if (arr instanceof ArrayBuffer) {
    return new Uint8Array(arr);
  }
};

const stringToBase64Url = (str) => {
  return btoa(str).replace(/\+/g, "-").replace(/\//g, "_").replace(/=+$/, "");
};
